#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  database_query.py
#
#  Copyright 2025 Mike Evans <mikee@saxicola.co.uk>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
# This will get the data from the DB and do something with it

import os
import sys
import csv
import MySQLdb
import MySQLdb.cursors
import warnings
import creds


def main():
    db = MySQLdb.connect(user=creds.user, host=creds.host, db=creds.db, \
                        passwd = creds.passwd, charset=creds.charset)
    cur = db.cursor(MySQLdb.cursors.DictCursor)
    numrows = cur.execute ("SELECT * FROM  events")
    events = cur.fetchall()
    #for event in events:
        #print(event['datetime'], event['card_num'], event['event'])
    with open(r'events.csv', 'a', newline='') as of:
        header = ['IDX','datetime','card_num','event']
        writer = csv.writer(of)
        writer.writerow(header)
        writer.writerows([d.values() for d in events])
    numrows = cur.execute ("SELECT card_num FROM  events group by card_num")
    cards = cur.fetchall()
    with open('cards.csv', 'w', newline='') as of:
        for card in cards: #f'{val:02}'
            try:
                print(f"{int(card['card_num']):010d}")
                of.write(f"{int(card['card_num']):010d}\n")
            except:
                pass


    return 0

if __name__ == '__main__':
    main()
