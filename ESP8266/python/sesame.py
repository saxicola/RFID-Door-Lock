#encoding; utf-8
'''
An ESP8266 micropython implementation of Sesame.
'''


import creds # Not on git for obvious reasons.
import esp
import gc
from machine import Pin, RTC
import network
import os
import sys
import socket
import time
from weigand import Wiegand
from umqtt.simple import MQTTClient

esp.osdebug(1)

DEBUG = print

# Need an array to store our cards.
# First card becomes master card
cards = []
mqtt = None
mqttc = None
wiegand = None

#enum  {READ,LEARN,SEND,MASTER,DELETE,STARTUP,BTTEST} mode; # C equivalent
class Mode():
    READ = 0
    LEARN = 1
    SEND = 2
    MASTER = 3
    DELETE = 4
    STARTUP = 5
    BTTEST = 6
mode = Mode.STARTUP

ap_name = "sesame" # Make this hidden?
passwd = "" # For access point. We get this from the master card.
cards_dat = 'cards.dat'
openings_dat = "openings.dat"

WIEGAND_ZERO = 14   # Pin number here. Pins will need voltage reducing.
WIEGAND_ONE = 12     # Pin number here

led = Pin(2, Pin.OUT) # Blue WiFi LED
LED_RED = Pin(15, Pin.OUT) #, Pin.PULL_DOWN)
LED_GRN = Pin(13, Pin.OUT) #, Pin.PULL_DOWN)
RELAY   = Pin(5, Pin.OUT)
#READER  = Pin(15, Pin.OUT, Pin.PULL_UP) # For the LED and beeper on the weigand reader. GND-Yellow = Beep.

'''
def sig_handler(sig, frame):
    INFO('Quitting on Ctrl+C')
    sys.exit(0)
signal.signal(signal.SIGINT, self.sig_handler)
'''
def on_mqqt_disconnect(self, client, userdata, rc):
    global mqtt
    client.ping()
    if rc != 0:
        print("Unexpected disconnection.")
    mqtt_connected = False

def sub_cb(topic, msg):
    print(topic, message)
    return


def mqtt_connect():
    global mqttc
    # mosquitto is running on ?? Using a systemd thingy
    mqtt_connected = None
    sta_if = network.WLAN(network.STA_IF)
    ipa = sta_if.ifconfig()[0].split('.')[3] # The last octet
    CLIENT_NAME = "sesame/"
    BROKER_ADDR = creds.MQTT_HOST
    mqttc = MQTTClient(CLIENT_NAME, BROKER_ADDR, port=1883, user=None, password=None, keepalive=60, ssl=False, ssl_params={})
    TOPIC = (CLIENT_NAME + str(ipa)).encode()
    #mqttc.set_callback(sub_cb)
    mqttc.on_disconnect = on_mqqt_disconnect
    try:
        print("Attempting to connect to MQQT broker at ".format(creds.MQTT_HOST))
        mqtt_connected = mqttc.connect()
    except:
        raise
    if mqtt_connected == 0:
        print("Connected to broker.")
        mqqt_publish('SYSTEM', 'REBOOT')
    else:
        print("Not connected to broker.")
    return mqttc


def mqqt_publish(card_num, event):
    """
    @param card_number
    @param String event type.
    """
    global mqttc
    print("MQTT publish")
    try:
        mqttc.publish( "sesame".encode(), "card_num: {} : event: {} ".format(str(card_num), event).encode(), qos=0)
    except OSError:
        mqttc = mqtt_connect()
        mqttc.publish( "sesame".encode(), "card_num: {} : event: {} ".format(str(card_num), event).encode(), qos=0)
    return


def file_or_dir_exists(filename):
    """ Check if if a  file or directory file_or_dir_exists"""
    try:
        os.stat(filename)
        return True
    except OSError:
        return False


def open_door(card_num):
    """ Unlock the door
    @param String either the IP address or the card number
    """
    now = time.localtime()
    print("OPENING THE DOOR")
    LED_GRN(1)
    RELAY(1)
    time.sleep(3) # Hold open for N seconds
    print("LOCKING THE DOOR")
    RELAY(0)
    LED_GRN(0)
    #with open(openings_dat, 'a') as fh:
    #    fh.write("{}-{:02d}-{:02d} {:02d}:{:02d} : {} : OPENED\n".format(now[0], now[1], now[2], now[3], now[4],card_num))


def get_cards(): # From disk
    cards = []
    with open(cards_dat, 'r') as fp:
        for line in fp:
            if line.strip():
                cards.append(int(line.rstrip('\n')))
    return cards


def write_cards(cards): # To disk
    with open(cards_dat, 'w') as fp:
        for card in cards:
            fp.write("{}\n".format(card))


def del_card(card_number):
    global mode
    print("Removing card {}".format(card_number,))
    cards = get_cards()
    if len(cards) == 0:
        mode = Mode.STARTUP # No master card listed! We have to start again.
        print("No cards!. Mode set to STARTUP")
        return
    print(cards)
    if card_number in cards: # If it is delete it.
        LED_RED(1)
        cards = [i for i in cards if i != card_number] # Remove the card.
        print("Card {} has been deleted.".format(card_number,))
        time.sleep(1)
        LED_RED(0)
    else:
        print("Card NOT deleted!")
    print(cards)
    write_cards(cards)


def on_card(card_number, facility_code, cards_read):
    global passwd, mode, cards
    now = time.localtime()
    print(mode)
    print("Card number: " + str(card_number))
    if mode == Mode.MASTER: # Remove or add a card.
        flash_leds(3)
        cards = get_cards()
        print(cards)
        # Is it already in the cards list?
        if card_number in cards[1:]: # If it is delete it. But not the master card!
            LED_RED(1)
            print("Card already exists.")
            cards = [i for i in cards if i != card_number] # Remove the card.
            print("Card has been deleted.")
            print(cards)
            mqqt_publish(card_number, "REMOVED")
            time.sleep(3)
            LED_RED(0)
        else: # Add a new card.
            LED_GRN(1)
            cards.append(card_number)
            print("Added new card: {}".format(card_number,))
            mqqt_publish(card_number, "ADDED")
            time.sleep(3)
            LED_GRN(0)
        write_cards(cards)
        mode = Mode.READ
        return
    if mode == Mode.STARTUP:
        # The first card we read is the master card and defines the access point password
        if len(cards)  == 0: ## Master card is the first card
            flash_leds(5)
            cards = []
            cards.append(card_number)
            # Write the new card list to file if it doesn't exist or it's empty
            if file_or_dir_exists(cards_dat):
                mode = Mode.READ
                on_card(card_number, facility_code, cards_read)
            else: #if file_or_dir_exists(cards_dat) == False:
                write_cards(cards)
                cards[0] = card_number
                print("Added new master card: {}".format(card_number,))
            mode = Mode.READ
            setup_AP(card_number)
            return
    if mode == Mode.READ:
        cards = get_cards()
        print(cards)
        if cards[0] == card_number: #cards.index(str(card_number)) == 0: ## Master card presented
            mode = Mode.MASTER
            print("PRESENT NEW CARD")
            flash_leds(3)
            return
        elif card_number in cards: # open the door.
            open_door(str(card_number))
            mqqt_publish(card_number, "OPENED")
        else:
            LED_RED(1)
            print("UNAUTHORISED CARD. THIS WILL BE LOGGED!")
            mqqt_publish(card_number, "REFUSED")
            with open(openings_dat, 'a') as fh:
                fh.write("{}-{:02d}-{:02d} {:02d}:{:02d} : {} : FAILED CARD!\n".format(now[0], now[1], now[2], now[3], now[4], card_number))
            time.sleep(2)
            LED_RED(0)
        return



def setup_AP():
    """ This would make a web page available to unlock the door from a 'phone.
    This will be a security risk if not implemented properly.
    The SSID is taken from the creds.py file and should be suitably obscure.
    The password is "sesame" + the serial number of the master card.
    TODO Maybe.
    """
    return
    global wiegand
    cards = get_cards()
    a,b = (wiegand.derive_num_facility(cards[0]))
    print(a,b)
    ap = network.WLAN(network.AP_IF)
    ap.active(True)
    ap.config(essid=str(a) + "s" + str(b), password=creds.pass_prefix + str(cards[0]))
    print(ap.ifconfig())
    while ap.active() == False:
        pass
    return ap.active()



def list_cards(): # To terminal?
    f = open(cards_dat, 'r')
    try:
        while(True):
            print(f.readline())
    except:
        f.close()


def start_webserver():
    # A page to control the door or just view settings.
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        s.bind(('', 80))
        s.listen(5)
        print('The webserver is up!')
    except OSError as e: # In case, there’s a socket left open, we’ll get an OS error, and reset the ESP32 with machine.reset(). This will “forget” the open socket.
        machine.reset
    return s

header = """<html>
    <head>
    <title>ESP Web Server</title> <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="data:,">
    <style>html{font-family: Helvetica; display:inline-block; margin: 0px auto; text-align: center;}
        h1{color: #0F3376; padding: 2vh;}p{font-size: 1.5rem;}
        .button{display: inline-block; background-color: #e7bd3b; border: none;
            border-radius: 4px; color: white; padding: 16px 40px; text-decoration: none; font-size: 30px; margin: 2px; cursor: pointer;}
        .button2{background-color: #4286f4;}
    </style>
    </head>
    """

def web_page():
    if RELAY.value() == 1:
        door_state="OPEN"
    else:
        door_state="LOCKED"

    html = header + """
        <body> <h1>ESP Web Server</h1>
            <p>DOOR IS: <strong> {} </strong></p><p><a href="/?led=on">
            <button class="button">OPEN</button></a></p>
            <!-- <p><a href="/?led=off">
             <button class="button button2">LOCK</button></a></p> -->
            <br/>
            """.format(door_state)
    return html



def cards_page(message):
    """ We need to be able to delete cards that are mislaid or stolen.
    Present a page to do this. Memory is limited though. Forebodings.
    """
    cards = []
    try:
        with open(cards_dat, 'r') as fp:
            for line in fp:
                if line.strip():
                    cards.append(int(line.rstrip('\n')))
    except:
        with open(cards_dat, 'w') as fp:
            pass
    print(cards)
    if len(cards) == 1:
        return "</body></html>"
    # Make a list with delete buttons.
    clist = "<h2>To remove a card click the appropriate button below<h2>\n"
    # Mark the master card
    clist += "Master card not shown.</br>\n"
    #clist += "<a href='/?c={}'><button name='{} ' type='submit' value='{}'>MASTER {}</button></a><br/>\n".format(str(cards[0]),str(cards[0]),str(cards[0]), str(cards[0]))
    for card in cards[1:]: # We don't want to list or delete the master card, usually.
        clist += "<a href='/?c={}'><button name='{}' type='submit' value='{}'>{}</button></a><br/>\n".format(str(card),str(card),str(card), str(card))
    html = """
    <body>
    {} \n<p>{}</p>\n
    </body></html>
    """.format(clist,message)
    return html



def web_admin():
    """ TODO
    """
    return


def flash_leds(i):
    """ Flash both LEDs
    @param integer, number fo flash cycles.
    """
    while i > 0:
        LED_GRN(1)
        time.sleep(0.2)
        LED_GRN(0)
        LED_RED(1)
        time.sleep(0.2)
        LED_RED(0)
        time.sleep(0.2)
        i -= 1


def wifi_connect():
    ''' Connect to home WiFi
    The problem then becomes how to find the IP address of this device?
    Name and password are taken frin the creds.py file.
    '''
    sta_if = network.WLAN(network.STA_IF)
    sta_if.active(True)
    sta_if.connect(creds.sta_ssid, creds.sta_password)
    while sta_if.isconnected() == False: # Wait for connection
        pass
    print('Connection successful')
    print(sta_if.ifconfig())
    return sta_if.isconnected()


def run():
    global mode, wiegand
    gc.collect()
    import micropython
    micropython.mem_info(1)
    wifi_connect()
    print("The WiFi has connected!")
    s = start_webserver()
    wiegand = Wiegand(WIEGAND_ZERO, WIEGAND_ONE, on_card)
    print("Reader is avalable!")
    # Flash the LEDs
    flash_leds(5)
    print("The door is avalable!")
    if file_or_dir_exists(cards_dat):
        mode = Mode.READ
        setup_AP()
    print("An access point is available")

    mqtt_connect()
    while(1):
        conn, addr = s.accept()
        print('Got a connection from %s' % str(addr))
        request = str(conn.recv(1024))
        print(request)
        led_on = request.find('/?led=on') # returns the position of the found string.
        led_off = request.find('/?led=off')
        card = request.find('/?c=')
        conn.send('HTTP/1.1 200 OK\n')
        conn.send('Content-Type: text/html\n')
        conn.send('Connection: close\n\n')
        if card == 6:
            card_num = int(request.split(' ')[1].replace('/?c=','')) # Extract the card number
            del_card(card_num) # Delete the card
            mqqt_publish(str(addr[0]), "DELETED")
        elif led_on == 6:
            print('DOOR OPEN')
            mqqt_publish(str(addr[0]), "OPENED")
            #conn.sendall(web_page())
            open_door(str(addr[0]))
        elif led_off == 6:
            print('DOOR LOCKED')
            RELAY(0)
            LED_RED(0)
        conn.sendall(web_page())
        conn.sendall(cards_page(''))
        conn.close()
        gc.collect()


if __name__ == "__main__":
    run()
